// Handle page not found errors

export function handle404(req, res, next) {
	const error = new Error('Page not found');
	error.status = 404;
	next(error);
}

// Handle application based errors like db or something broken 

export function handleGlobalErrors(error, req, res, next) {
	res.status(error.status || 500)
		.json({
			error: error.message
		});
}