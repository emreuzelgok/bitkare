import bodyParser from 'body-parser';
import morgan from 'morgan';
import { handle404, handleGlobalErrors } from './ErrorHandling';

/**
 * This function setting up middlewares to expess application.
 * It's handle some coditions processing on request.
 * @param {Object} app - Express Instance
 */

function setupMiddlewares (app) {
	// Using some middlewares for processing json data and logging.
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());
	app.use(morgan('dev'));
}

/**
 * This function setting up tail middlewares to expess application.
 * It's handle some coditions before sending responses.
 * Eg: application based errors.
 * @param {Object} - app Express Instance
 */

function setupTailMiddlewares (app) {
	app.use(handle404);
	app.use(handleGlobalErrors);
}

export { setupMiddlewares, setupTailMiddlewares };