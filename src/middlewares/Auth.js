import jwt from 'jsonwebtoken';
import configuration from '../../config';

const env = process.env.NODE_ENV || 'development';
const config = configuration[env];

/**
 * Auth middleware for checking user is logged in.
 */

export default (req, res, next) => {
	try {
		// Trying to get authentication token from request headers.
		const token = req.headers.authorization.split(' ')[1];

		// Trying to verify and decode user data from token.
		const user = jwt.verify(token, config.jwt_secret);

		// Setting up user on request. It can use for checking user is owner or another security check.
		req.user = user;
		next();
	} catch (error) {
    return res.status(401).json({
      message: 'Auth failed'
    });
  }
};