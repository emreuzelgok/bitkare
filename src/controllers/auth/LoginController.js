import User from '../../models/User';
import bcrypt from 'bcrypt';
import { generateToken } from '../../utils';

function LoginController ({ body }, res, next) {
	const { email, password } = body;

	// Checking required properties
	if (!email || !password) {
		res.status(400).json({
			message: 'Please fill all inputs'
		});
	}

	// Checking user exists
	User.findOne({ email })
		.then(
			(result) => {
				// Check user password
				if (result.authenticate(password)) {
					const user = result.toObject();
					// User converting to javascript object
					delete user.password;
					// Removing password property for security
					const token = generateToken(user);
					res.status(200).json({
						message: 'login success',
						token,
						user
					});	
				} else {
					res.status(401).json({
						message: 'Password does not match'
					});
				}
			},
			(error) => {
				// User does not exists
				res.status(404).json({
					message: 'user not found',
					error: (error.errors || error)
				});
			}
		);
}

export default LoginController;