import User from '../../models/User';

function RegisterController ({ body }, res, next) {
	// Fill user model from request body
	// Validations in the User model
	const user = new User(body);

	// Trying to save user
	user.save()
		.then(
			(result) => {
				// User has saved.
				const user = result.toObject();
				// User converting to javascript object
				delete user.password;
				// Removing password property for security

				// Send response
				res.status(200).json({
					message: 'user created',
					user
				});
			},
			(error) => {
				res.status(400).json({
					message: 'user create error',
					error: (error.errors || error)
				});
			}
		)
}

export default RegisterController;