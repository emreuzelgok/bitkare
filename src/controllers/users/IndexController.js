import User from '../../models/User';

function IndexController (req, res, next) {
	// Find all users
	User.find()
		.select('_id fullname email username')
		.then(
			(data) => {
				// send userdate with response
				res.status(200).json({
					users: data
				});
			},
			(err) => {
				// handle error
				res.status(500);
			}
		);
}

export default IndexController;