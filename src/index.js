import 'babel-polyfill';
import express from 'express';
import setupRoutes from './routes';
import { setupMiddlewares, setupTailMiddlewares } from './middlewares';
import configuration from '../config';
import mongoose from 'mongoose';
// Setup express

// Getting environment configuration 
const env = process.env.NODE_ENV || 'development';
const config = configuration[env];
const app = express();

// Database connection
mongoose
	.connect(config.db)
	.then(
		() => {},
		(error) => console.error('Database connection error')
	);

app.use(function (req, res, next) {
	req.jwtSecret = config.jwt_secret;
	next();
});

// Setting up head middlewares.
setupMiddlewares(app);

// Setting up application routes.
setupRoutes(app);

// Setting up tail middlewares.
setupTailMiddlewares(app);

app.listen(config.port);
console.log(`Listening on port ${config.port}...`);