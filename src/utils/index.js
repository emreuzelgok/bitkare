import jwt from 'jsonwebtoken';
import configuration from '../../config';

const env = process.env.NODE_ENV || 'development';
const config = configuration[env];

/**
 * This function creating a JWT token with user data.
 * @param  {Object} user
 * @return {String}
 */

export function generateToken(user) {
  const { email, _id:userId } = user;
	return jwt.sign({ email, userId }, config.jwt_secret, { expiresIn: "1h"});
}