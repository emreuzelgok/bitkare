import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import { isEmail, isAlphanumeric } from 'validator';
import bcrypt from 'bcrypt';
const { Schema } = mongoose;

/**
 * Definition User model and validations
 */

const userSchema = new Schema({
	email: {
    type: String,
    unique: true,
    reqired: 'Email is required.',
    default: '',
    validate: [isEmail, 'Please fill a valid emial address.'],
    trim: true
  },
  username: {
    type: String,
    unique: true,
    minlength: [5, 'Username should be greater then 5 characters'],
    reqired: 'Usename is required.',
    default: '',
    trim: true
  },
	fullname: {
    type: String,
    required: 'Fullname is reqired',
    minlength: [5, 'Fullname should be greater then 5 characters'],
    maxlength: [60, 'Fullname should be lower then 60 characters'],
    default: '',
    trim: true,
  },
  password: {
    type: String,
    reqired: 'Password is required.',
    default: '',
    minlength: [8, 'Password should be greater then 8 characters'],
    maxlength: [60, 'Password should be lower then 60 characters'],
    trim: true
  }
});

// Add authenticate method to Model for checking password match
userSchema.methods.authenticate = function (password) {
  return bcrypt.compareSync(password, this.password);
};

// Hash password before save
userSchema.pre('save', function (next) {
	this.password = bcrypt.hashSync(this.password, 10);
	next();
});

// override findOne method for checking user is exist
userSchema.post('findOne', function(user, next) {
  if (!user) {
    return next(new Error('user not found!'));
  }
  next();
});

userSchema.plugin(uniqueValidator);


export default mongoose.model('User', userSchema);