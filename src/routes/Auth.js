import express from 'express';
import RegisterController from '../controllers/auth/RegisterController';
import LoginController from '../controllers/auth/LoginController';
const router = express.Router();

/**
 * Definition for auth Endpoins with route level middleware.
 * More info: https://expressjs.com/en/guide/using-middleware.html#middleware.router
 * Current base path '/auth'
 */

// Handle incoming POST requests to '/login'
router.post('/login', LoginController);

// Handle incoming POST requests to '/register'
router.post('/register', RegisterController);

export default router;