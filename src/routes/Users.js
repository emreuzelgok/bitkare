import express from 'express';
import IndexController from '../controllers/users/IndexController';
const router = express.Router();

/**
 * Definition for auth endpoints with route level middleware.
 * More info: https://expressjs.com/en/guide/using-middleware.html#middleware.router
 * Current base path '/users'
 */

// Handle incoming GET requests to '/'
router.get('/', IndexController);

export default router;