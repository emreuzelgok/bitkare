import AuthRoutes from './Auth';
import UserRoutes from './Users';
import AuthMiddleware from '../middlewares/Auth';

const setupRoutes = (app) => {

	/**
	 * Definition all base routes for endpoins with express application level middleware.
	 * More info: https://expressjs.com/en/guide/using-middleware.html#middleware.application
	 */
	
	app.use('/auth', AuthRoutes);
	app.use('/users', AuthMiddleware, UserRoutes);
}

export default setupRoutes;